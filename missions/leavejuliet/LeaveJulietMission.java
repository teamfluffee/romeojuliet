package scripts.modules.romeojuliet.missions.leavejuliet;

import org.tribot.api2007.Game;
import org.tribot.api2007.Player;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.romeojuliet.RomeoAndJuliet;
import scripts.modules.romeojuliet.missions.leavejuliet.decision.ShouldWalkToStairs;

public class LeaveJulietMission implements TreeMission {
    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWalkToStairs();
    }

    @Override
    public String getMissionName() {
        return "Leaving Juliet";
    }

    @Override
    public String getBagId() {
        return BagIds.ROMEO_AND_JULIET.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Player.getPosition().getPlane() == 1 &&
                (Game.getSetting(RomeoAndJuliet.QUEST_SETTING) == 20 ||
                        Game.getSetting(RomeoAndJuliet.QUEST_SETTING) == 60);
    }

    @Override
    public boolean isMissionCompleted() {
        return Player.getPosition().getPlane() != 1 &&
                (Game.getSetting(RomeoAndJuliet.QUEST_SETTING) == 20 ||
                        Game.getSetting(RomeoAndJuliet.QUEST_SETTING) == 60);
    }
}

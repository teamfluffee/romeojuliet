package scripts.modules.romeojuliet.missions.talkjuliet.decision;

import org.tribot.api2007.NPCs;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSTile;
import scripts.fluffeesapi.game.genericProcessNodes.TalkToGuide;
import scripts.fluffeesapi.game.genericProcessNodes.WalkToLocation;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;

public class ShouldWalkToJuliet extends ConstructorDecisionNode {
    @Override
    public boolean isValid() {
        return NPCs.findNearest("Juliet").length < 1 || (Player.getPosition().getY() < 4000 &&
                Player.getPosition().getY() > 3426);
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WalkToLocation(new RSTile(3157, 3426, 1), "Walking to Juliet"));
        setFalseNode(new TalkToGuide("Juliet", "Talking to Juliet",
                new String[]{"Yes I've met him.", "Certainly, I'll do so straight away."}));
    }
}

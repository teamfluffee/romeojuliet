package scripts.modules.romeojuliet.missions.talkjuliet.decision;

import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSTile;
import scripts.fluffeesapi.game.genericProcessNodes.NavigateChangePlaneObject;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;

public class ShouldClimbUpStairs extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Player.getPosition().getPlane() == 0;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new NavigateChangePlaneObject("Staircase", new RSTile(3156, 3435, 0),
                "Climb-up", "Climbing up stairs"));
        setFalseNode(new ShouldWalkToJuliet());
    }

}

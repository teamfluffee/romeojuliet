package scripts.modules.romeojuliet.missions.talkjuliet;

import org.tribot.api2007.Game;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.NPCs;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.romeojuliet.RomeoAndJuliet;
import scripts.modules.romeojuliet.missions.talkjuliet.decision.ShouldWalkToDownstairs;

public class TalkJulietMission implements TreeMission {
    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWalkToDownstairs();
    }

    @Override
    public String getMissionName() {
        return "Talking to Juliet";
    }

    @Override
    public String getBagId() {
        return BagIds.ROMEO_AND_JULIET.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(RomeoAndJuliet.QUEST_SETTING) < 20 ||
                (Game.getSetting(RomeoAndJuliet.QUEST_SETTING) == 50 &&
                        (Inventory.getCount("Cadava potion") > 0 ||
                                (Game.getSetting(1021) > 2048 && NPCs.find("Juliet").length > 0)));
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(RomeoAndJuliet.QUEST_SETTING) >= 20 &&
                Game.getSetting(RomeoAndJuliet.QUEST_SETTING) != 50;
    }
}

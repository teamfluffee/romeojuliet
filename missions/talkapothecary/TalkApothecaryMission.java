package scripts.modules.romeojuliet.missions.talkapothecary;

import org.tribot.api2007.Game;
import org.tribot.api2007.Inventory;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.romeojuliet.RomeoAndJuliet;
import scripts.modules.romeojuliet.missions.talkapothecary.decisions.ShouldTalkToApothecary;

public class TalkApothecaryMission implements TreeMission {
    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldTalkToApothecary();
    }

    @Override
    public String getMissionName() {
        return "Talk to Apothecary";
    }

    @Override
    public String getBagId() {
        return BagIds.ROMEO_AND_JULIET.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(RomeoAndJuliet.QUEST_SETTING) == 40 ||
                (Game.getSetting(RomeoAndJuliet.QUEST_SETTING) == 50 && Inventory.getCount("Cadava berries") > 0);
    }

    @Override
    public boolean isMissionCompleted() {
        return (Game.getSetting(RomeoAndJuliet.QUEST_SETTING) == 50 &&
                (Inventory.getCount("Cadava potion") > 0 || Inventory.getCount("Cadava berries") < 1));
    }
}

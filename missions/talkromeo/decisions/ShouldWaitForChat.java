package scripts.modules.romeojuliet.missions.talkromeo.decisions;

import scripts.fluffeesapi.client.clientextensions.Game;
import scripts.fluffeesapi.client.clientextensions.NPCChat;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.romeojuliet.sharednodes.WatchCutscene;

public class ShouldWaitForChat extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getVarBitValue(6719) > 0 && !NPCChat.inChat(1);
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WatchCutscene());
        setFalseNode(new ShouldTalkToRomeo());
    }

}

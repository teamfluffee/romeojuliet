package scripts.modules.romeojuliet.missions.talkromeo.decisions;

import org.tribot.api2007.NPCs;
import org.tribot.api2007.types.RSTile;
import scripts.fluffeesapi.game.genericProcessNodes.TalkToGuide;
import scripts.fluffeesapi.game.genericProcessNodes.WalkToLocation;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;

public class ShouldTalkToRomeo extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return NPCs.findNearest("Romeo").length > 0;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new TalkToGuide("Romeo", "Talking to Romeo",
                new String[]{"Ok, thanks."}));
        setFalseNode(new WalkToLocation(new RSTile(3212, 3428, 0), "Walking to Romeo"));
    }

}

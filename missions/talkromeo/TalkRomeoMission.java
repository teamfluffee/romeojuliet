package scripts.modules.romeojuliet.missions.talkromeo;

import org.tribot.api2007.Game;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.romeojuliet.RomeoAndJuliet;
import scripts.modules.romeojuliet.missions.talkromeo.decisions.ShouldWaitForChat;

public class TalkRomeoMission implements TreeMission {
    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWaitForChat();
    }

    @Override
    public String getMissionName() {
        return "Talk to Romeo";
    }

    @Override
    public String getBagId() {
        return BagIds.ROMEO_AND_JULIET.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(RomeoAndJuliet.QUEST_SETTING) == 20 ||
                Game.getSetting(RomeoAndJuliet.QUEST_SETTING) == 60;
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(RomeoAndJuliet.QUEST_SETTING) > 20 &&
                Game.getSetting(RomeoAndJuliet.QUEST_SETTING) != 60;
    }
}

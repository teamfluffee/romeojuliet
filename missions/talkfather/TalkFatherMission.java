package scripts.modules.romeojuliet.missions.talkfather;

import org.tribot.api2007.Game;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.romeojuliet.RomeoAndJuliet;
import scripts.modules.romeojuliet.missions.talkfather.decisions.ShouldTalkToFather;

public class TalkFatherMission implements TreeMission {
    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldTalkToFather();
    }

    @Override
    public String getMissionName() {
        return "Talk to Father";
    }

    @Override
    public String getBagId() {
        return BagIds.ROMEO_AND_JULIET.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(RomeoAndJuliet.QUEST_SETTING) == 30;
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(RomeoAndJuliet.QUEST_SETTING) > 30;
    }
}

package scripts.modules.romeojuliet.missions.pickberries.decisions;

import org.tribot.api2007.types.RSTile;
import scripts.fluffeesapi.client.clientextensions.Objects;
import scripts.fluffeesapi.game.genericProcessNodes.WalkToLocation;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.romeojuliet.missions.pickberries.processes.PickCadavaBerries;

public class ShouldWalkCadavaBush extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Objects.find(5, "Cadava bush").length < 1;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WalkToLocation(new RSTile(3269, 3370, 0), "Walking to Cadava Bush"));
        setFalseNode(new PickCadavaBerries());
    }

}

package scripts.modules.romeojuliet.missions.pickberries;

import org.tribot.api2007.Game;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.romeojuliet.RomeoAndJuliet;
import scripts.modules.romeojuliet.missions.pickberries.decisions.ShouldWalkVarrockSquare;

public class PickBerriesMission implements TreeMission {
    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWalkVarrockSquare();
    }

    @Override
    public String getMissionName() {
        return "Pick berries";
    }

    @Override
    public String getBagId() {
        return BagIds.ROMEO_AND_JULIET.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(RomeoAndJuliet.QUEST_SETTING) == 50 &&
                Inventory.getCount("Cadava berries", "Cadava potion") < 1 &&
                Game.getSetting(1021) <= 2048;
    }

    @Override
    public boolean isMissionCompleted() {
        return Inventory.getCount("Cadava berries") > 0;
    }
}

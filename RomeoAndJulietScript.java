package scripts.modules.romeojuliet;

import org.tribot.script.interfaces.Painting;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.Mission;
import scripts.fluffeesapi.scripting.frameworks.mission.scriptTypes.MissionScript;
import scripts.fluffeesapi.scripting.painting.scriptPaint.ScriptPaint;
import scripts.fluffeesapi.scripting.swingcomponents.gui.AbstractWizardGui;

public class RomeoAndJulietScript extends MissionScript implements Painting {

    private ScriptPaint scriptPaint = new ScriptPaint.Builder(
                ScriptPaint.hex2Rgb("#ff0054"), "Romeo and Juliet")
            .addField("Version", Double.toString(1.00))
            .build();

    @Override
    public Mission getMission() {
        return new RomeoAndJuliet();
    }

    @Override
    public AbstractWizardGui getGUI() {
        return null;
    }

    @Override
    public ScriptPaint getScriptPaint() {
        return scriptPaint;
    }
}

package scripts.modules.romeojuliet;

import org.tribot.api2007.Game;
import org.tribot.api2007.Login;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.Mission;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.MissionManager;
import scripts.modules.romeojuliet.missions.leavejuliet.LeaveJulietMission;
import scripts.modules.romeojuliet.missions.pickberries.PickBerriesMission;
import scripts.modules.romeojuliet.missions.talkapothecary.TalkApothecaryMission;
import scripts.modules.romeojuliet.missions.talkfather.TalkFatherMission;
import scripts.modules.romeojuliet.missions.talkjuliet.TalkJulietMission;
import scripts.modules.romeojuliet.missions.talkromeo.TalkRomeoMission;

import java.util.LinkedList;

public class RomeoAndJuliet implements MissionManager {

    public static int QUEST_SETTING = 144;
    private LinkedList<Mission> missionList = new LinkedList<>();

    @Override
    public LinkedList<Mission> getMissions() {
        if (missionList.isEmpty()) {
            missionList.add(new TalkJulietMission());
            missionList.add(new LeaveJulietMission());
            missionList.add(new TalkRomeoMission());
            missionList.add(new TalkFatherMission());
            missionList.add(new TalkApothecaryMission());
            missionList.add(new PickBerriesMission());
        }
        return missionList;
    }

    @Override
    public boolean shouldLoopMissions() {
        return true;
    }

    @Override
    public String getMissionName() {
        return "Romeo and Juliet";
    }

    @Override
    public String getBagId() {
        return BagIds.ROMEO_AND_JULIET.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Login.getLoginState() == Login.STATE.INGAME && Game.getSetting(QUEST_SETTING) <= 100;
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(QUEST_SETTING) >= 100;
    }
}
